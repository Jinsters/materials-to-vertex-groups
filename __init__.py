# MATERIALS TO VERTEX GROUPS

bl_info = {
	"name": "Materials to Vertex Groups",
	"author": "Jinsters",
	"version": (0, 1, 0),
	"blender": (2, 79, 0),
	"location": "Properties > Materials > Dropdown Menu",
	"description": "Converts all material assignments to vertex groups",
	"warning": "",
	"wiki_url": "",
	"category": "Object",
	}

import bpy
from bpy import (data, context, ops)


def add_to_menu(self, context):
    self.layout.separator()
    self.layout.operator(MaterialsToVertexGroups.bl_idname, text="Materials to Vertex Groups", icon="GROUP_VERTEX")


class MaterialsToVertexGroups(bpy.types.Operator):
	bl_idname = 'object.materials_to_vertex_groups'
	bl_label = 'Materials to Vertex Groups'
	bl_description = 'Converts all material assignments to vertex groups'
	bl_options = {'REGISTER', 'UNDO'}
	
	def execute(self, context):
		obj = context.active_object

		for index, slot in enumerate(obj.material_slots):
			# Select verts from faces with material index
			if not slot.material:
				continue

			verts = [v for f in obj.data.polygons if f.material_index == index for v in f.vertices]

			if len(verts):
				# Add vertex group, if it doesn't already exist
				vg = obj.vertex_groups.get(slot.material.name)

				if vg is None: 
					vg = obj.vertex_groups.new(name=slot.material.name)

				vg.add(verts, 1.0, 'ADD')

		return {"FINISHED"}		
				
		
def register():
	bpy.utils.register_module(__name__)
	bpy.types.MATERIAL_MT_specials.append(add_to_menu)

def unregister():
	bpy.utils.unregister_module(__name__)
	bpy.types.MATERIAL_MT_specials.remove(add_to_menu)

if __name__ == "__main__":
	register()
	
	# test call
	# bpy.ops.object.materials_to_vertex_groups()
